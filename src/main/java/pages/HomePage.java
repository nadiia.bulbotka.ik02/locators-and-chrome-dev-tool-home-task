package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{
    public HomePage(WebDriver driver){
        super(driver);
    }
    public void openURL(final String url){
        driver.get(url);
        waitForPageLoadComplete(WAIT_TIME);
    }

   @FindBy(className = "_2yPTK9xQ")
   private WebElement login;

   @FindBy(css = "input[type = 'password']")
   private WebElement password;

   @FindBy(xpath = "//button[@class='Ol0-ktls jY4tHruE _2Qy_WiMj']")
   private WebElement logInButton;

   @FindBy(xpath = "//button[text() = 'Написати листа']")
   private WebElement writeALetterButton;

   @FindBy(css = "input[name = 'toFieldInput']")
   private WebElement ToMail;

   @FindBy(name = "subject")
   private WebElement textLetter;

   @FindBy(xpath = "//div[@class='sendmsg__bottom-controls']//button[@class='button primary send']")
   private WebElement sendAMessageButton;

   public void inputLogInData(final String log, final String pword){
       login.sendKeys(log);
       password.sendKeys(pword);
       ((JavascriptExecutor) driver).executeScript("arguments[0].click()", logInButton);
       waitForPageLoadComplete(WAIT_TIME);
       waitForAjaxToComplete(WAIT_TIME);
   }
   public void clickWriteSomeLetter(){
       writeALetterButton.click();
       waitForAjaxToComplete(WAIT_TIME);
   }
   public void writeYourLetter(final String to, final String text){
       ToMail.sendKeys(to);
       textLetter.sendKeys(text);
       ((JavascriptExecutor) driver).executeScript("arguments[0].click()", sendAMessageButton);
   }
}
