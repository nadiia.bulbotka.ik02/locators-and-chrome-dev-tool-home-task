package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class StepDefinitions {
    PageFactoryManager pageFactoryManager;
    HomePage homePage;
    WebDriver driver;
    @Before
    public void testsSetup()
    {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }
    @Given("User opens {string}")
    public void openHomePage(final String URL){
        homePage = pageFactoryManager.getHomePage();
        homePage.openURL(URL);
    }

    @When("User inputs {string} and {string}")
    public void userInputsLoginAndPassword(final String log, final String pasw) {
        homePage.inputLogInData(log, pasw);
    }

    @And("User clicks writeLetter button")
    public void userClicksWriteLetterButton() {
        homePage.clickWriteSomeLetter();
    }

    @Then("User inputs {string} and some {string}")
    public void userInputsReceiverAndSomeMessageText(final String to, final String message) {
        homePage.writeYourLetter(to, message);
    }
    @After
    public void tearDown(){
        driver.close();
    }
}
