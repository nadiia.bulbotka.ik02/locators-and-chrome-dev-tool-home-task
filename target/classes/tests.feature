Feature: Tasks
  As a user
  I want to test some email functionality

  Scenario Outline: User sends a message for some another user
    Given User opens '<homePage>'
    When User inputs '<login>' and '<password>'
    And User clicks writeLetter button
    Then User inputs '<receiver>' and some '<messageText>'

    Examples:
    |homePage                                                                                | login            | password  | receiver               | messageText       |
    | https://accounts.ukr.net/login?client_id=9GLooZH9KjbBlWnuLkVX&drop_reason=logout       |  itistestbox     | foronetime| nadjabulbotka@gmail.com| it is a test hello|
